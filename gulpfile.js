'use strict';

/**
 * Main gulpfile
 * @see https://gulp.readme.io/docs/gulpseriestasks
 * @see https://gulp.readme.io/docs/gulpparalleltasks
 * @see https://gulp.readme.io/docs/gulpwatchglob-opts-fn
 * */
var gulp = require('gulp');
var paths = require('./gulp-tasks/paths');

/**
 * Lazy load tasks
 * @param {string} taskName
 * @param {string} path
 * @param {Object} options
 * */
function lazyRequireTask(taskName, path, options) {
    options = options || {};
    options.taskName = taskName;
    gulp.task(taskName, function(callback) {
        var task = require(path).call(this, options);

        return task(callback);
    });
}

/**
 * Pug
 * */
lazyRequireTask('pug', './gulp-tasks/pug', {
    src: paths.src + '/pug/*.pug',
    dest: paths.dest + '/'
});

/**
 * SASS
 * */
lazyRequireTask('sass', './gulp-tasks/sass', {
    src: paths.src + '/sass/*.scss',
    dest: paths.dest + '/css/'
});

/**
 * Autoprefixer
 * @see https://github.com/sindresorhus/gulp-autoprefixer
 * */
lazyRequireTask('autoprefixer', './gulp-tasks/autoprefixer', {
    src: paths.dest + '/css/*.css',
    dest: paths.dest + '/css/'
});

/**
 * Clean
 * @see https://github.com/peter-vilja/gulp-clean
 * */
lazyRequireTask('clean', './gulp-tasks/clean', {
    src: [
        paths.dest + '/css/*.css',
        paths.dest + '/*.html',
        paths.dest + '/js/*.js'
    ]
});

/**
 * Concat js
 * @see https://github.com/contra/gulp-concat
 * */
lazyRequireTask('concat', './gulp-tasks/concat', {
    src: [
        paths.src + '/js/popup.js',
        paths.src + '/js/map.js',
        paths.src + '/js/slider.js',
        paths.src + '/js/sticky-header.js',
        paths.src + '/js/sticky-sidebar.js',
        paths.src + '/js/dropdown.js',
        paths.src + '/js/timer.js',
        paths.src + '/js/scroll-animation.js',
        paths.src + '/js/sticker.js',
        paths.src + '/js/collapse.js',
        paths.src + '/js/scroll-menu.js',
        paths.src + '/js/input-mask.js',
        paths.src + '/js/mobile-menu.js',
        paths.src + '/js/browser-definition.js',
        paths.src + '/js/responsive-svg.js',
        paths.src + '/js/additional-text.js',
        paths.src + '/js/feedback-popup.js',
        paths.src + '/js/app.js'
    ],
    outputFileName: 'scripts',
    dest: paths.dest + '/js/'
});

/**
 * Javascript copy
 * */
gulp.task('js:copy', function() {
    return gulp.src([
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/jquery-colorbox/jquery.colorbox-min.js',
            './node_modules/bxslider/dist/jquery.bxslider.min.js',
            './node_modules/sticky-kit/dist/sticky-kit.min.js',
            './node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js'
        ])
        .pipe(gulp.dest(paths.dest + '/js/'));
});

/**
 * Concat all js
 * */
lazyRequireTask('concat:all:js', './gulp-tasks/concat', {
    src: [
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/bxslider/dist/jquery.bxslider.min.js',
        './node_modules/sticky-kit/dist/sticky-kit.min.js',
        './node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js',
        paths.dest + '/js/scripts.js'
    ],
    outputFileName: 'scripts.concat',
    dest: paths.dest + '/js/'
});

/**
 * Watcher
 * @see https://gulp.readme.io/docs/gulpwatchglob-opts-fn
 * */
gulp.task('watch', function() {
    gulp.watch([
        paths.src + '/pug/**/*.pug',
        paths.src + '/pug/**/*.svg'
    ], gulp.series('pug'));
    gulp.watch(paths.src + '/sass/**/*.scss', gulp.series('sass', 'autoprefixer'));
    gulp.watch(paths.src + '/js/*.js', gulp.parallel('concat', 'js:copy'));
});

/**
 * Starting browsers synchronize, local server, live reloading
 * @see https://browsersync.io/docs/gulp
 * @see https://browsersync.io/docs/options
 * @see https://browsersync.io/docs/options#option-server
 * @see https://browsersync.io/docs/options#option-port
 * @see https://browsersync.io/docs/options#option-watchOptions
 * */
lazyRequireTask('serve', './gulp-tasks/serve', {
    server: [
        paths.dest
    ],
    port: 8000,
    watch: [
        paths.dest + '/**/*.*'
    ]
});

/**
 * Start build WITHOUT watcher and browser sync
 * */
gulp.task('build', gulp.series('clean', 'sass', 'autoprefixer', gulp.parallel('concat', 'js:copy'), 'pug'));

/**
 * Start build WITH watcher
 * */
gulp.task('default', gulp.series('build', gulp.parallel('watch', 'serve')));