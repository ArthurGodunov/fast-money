var popup = (function () {
    "use strict";

    var DATA_ATTR_POPUP = 'data-popup',
        DATA_ATTR_POPUP_OVERLAY = 'data-popup-overlay',
        DATA_ATTR_POPUP_CLOSE = 'data-popup-close',
        DATA_ATTR_POPUP_LINK = 'data-popup-link',
        CLASS_NAME_OPEN = 'open';

    var openPopup = function (popup) {
        $('[' + DATA_ATTR_POPUP_LINK + ']').each(function () {
            $(this).on('click', function () {
                $('[' + DATA_ATTR_POPUP_OVERLAY + '="' + popup + '"]').addClass(CLASS_NAME_OPEN);
            });
        });
    };

    var initPopup = function () {
        var $popup = $('[' + DATA_ATTR_POPUP + ']');

        if ($popup.length) {
            $('[' + DATA_ATTR_POPUP_CLOSE + ']').on('click', function () {
                $(this).parents('[' + DATA_ATTR_POPUP_OVERLAY + ']').removeClass(CLASS_NAME_OPEN);
            });

            $popup.on('click', function (event) {
                event.stopPropagation();
            });

            $('[' + DATA_ATTR_POPUP_OVERLAY + ']').on('click', function () {
                $(this).removeClass(CLASS_NAME_OPEN);
            });

            $('[' + DATA_ATTR_POPUP_LINK + ']').each(function () {
                var $this = $(this),
                    popup = $this.attr(DATA_ATTR_POPUP_LINK);

                $this.on('click', function () {
                    $('[' + DATA_ATTR_POPUP_OVERLAY + '="' + popup + '"]').addClass(CLASS_NAME_OPEN);
                });
            });
        }
    };

    return {
        initPopup: initPopup,
        openPopup: openPopup
    }
})();
var map = (function () {
    "use strict";

    var ID_REGIONS = '#region-minsk, #region-grodno, #region-brest, #region-gomel, #region-mogilev, #region-vitebsk',
        CLASS_NAME_HOVER_REGION = 'active';

    var initMap = function () {
        $(ID_REGIONS).each(function () {
            var $this = $(this);

            $this.hover(
                function () {
                    $this.addClass(CLASS_NAME_HOVER_REGION);
                },
                function () {
                    $this.removeClass(CLASS_NAME_HOVER_REGION);
                }
            );
        });
    };

    return {
        initMap: initMap
    }
})();
var slider = (function () {
    "use strict";

    var DATA_ATTR_SLIDER = 'data-slider',
        SCREEN_LG = 1024;

    /**
     * @see https://github.com/stevenwanderski/bxslider-4
     * */
    var initBxSlider = function () {
        var $slider = $('[' + DATA_ATTR_SLIDER + ']');

        if ($slider.length) {
            $slider.bxSlider({
                speed: 500,
                auto: false,
                pause: 4000,
                pager: false,
                nextText: '',
                prevText: '',
                touchEnabled: $(window).width() < SCREEN_LG
            });
        }
    };

    return {
        initSlider: initBxSlider
    }
})();
var stickyHeader = (function () {
    "use strict";

    var DATA_ATTR_HEADER_TOP = 'data-header-top',
        DATA_ATTR_HEADER_BOTTOM = 'data-header-bottom',
        CLASS_NAME_STICKY = 'sticky',
        SCREEN_WIDTH_LG = 1024;

    var initStickyHeader = function () {
        var $headerTop = $('[' + DATA_ATTR_HEADER_TOP + ']'),
            $headerBottom = $('[' + DATA_ATTR_HEADER_BOTTOM + ']'),
            headerTopHeight = $headerTop.height(),
            windowScrollY = window.pageYOffset;

        if ($(window).width() > SCREEN_WIDTH_LG - 1) {
            if (windowScrollY >= headerTopHeight) {
                $headerBottom.addClass(CLASS_NAME_STICKY);
            } else {
                $headerBottom.removeClass(CLASS_NAME_STICKY);
            }
        } else {
            $headerBottom.removeClass(CLASS_NAME_STICKY);
        }
    };

    return {
        DATA_ATTR_HEADER_TOP: DATA_ATTR_HEADER_TOP,
        DATA_ATTR_HEADER_BOTTOM: DATA_ATTR_HEADER_BOTTOM,
        initStickyHeader: initStickyHeader
    }
})();
var stickySidebar = (function () {
    "use strict";

    var DATA_STICKY_SIDEBAR = 'data-sticky-sidebar',
        CLASS_NAME_HEADER_NAV = 'header-nav';

    var initStickySidebar = function () {
        $('[' + DATA_STICKY_SIDEBAR + ']').each(function () {
            $(this).stick_in_parent({
                offset_top: $('[' + stickyHeader.DATA_ATTR_HEADER_BOTTOM + ']').find('.' + CLASS_NAME_HEADER_NAV).height() + 20
            });
        });
    };

    return {
        initStickySidebar: initStickySidebar
    }
})();
var dropdown = (function () {
    "use strict";

    var DATA_ATTR_DROPDOWN = 'data-dropdown',
        DATA_ATTR_DROPDOWN_LINK = 'data-dropdown-link';

    var toogleDropdown = function () {
        var $link = $('[' + DATA_ATTR_DROPDOWN_LINK + ']');

        if ($link.length) {
            $link.on('click', function (event) {
                var $self = $(this);

                $self.parents('[' + DATA_ATTR_DROPDOWN + ']').toggleClass('open');

                event.stopPropagation();

                $('body').on('click.dropdown', function (eventBody) {
                    if (!$(eventBody.target).closest('[' + DATA_ATTR_DROPDOWN + ']').length) {
                        $self.closest('[' + DATA_ATTR_DROPDOWN + ']').removeClass('open');

                        $('body').off('click.dropdown');
                    }
                });
            });
        }
    };

    return {
        initDropdown: toogleDropdown
    }
})();
var timer = (function () {
    "use strict";

    var DATA_ATTR_INCREASE_NUMBER = 'data-increase-number',
        DATA_ATTR_TIMER_DAY = 'data-timer-day',
        DATA_ATTR_TIMER_HOUR = 'data-timer-hour',
        DATA_ATTR_TIMER_MIN = 'data-timer-min',
        DATA_ATTR_TIMER_SEC = 'data-timer-sec',
        DATA_ATTR_TIMER_DAY_UNIT = 'data-timer-day-unit',
        DATA_ATTR_TIMER_HOUR_UNIT = 'data-timer-hour-unit',
        DATA_ATTR_TIMER_MIN_UNIT = 'data-timer-min-unit',
        DATA_ATTR_TIMER_SEC_UNIT = 'data-timer-sec-unit',
        START_DATE_INCREASE_NUMBER = new Date(2017, 6, 6, 0, 0, 0, 0); // 6 July 2017 00:00:00

    /**
     * The value of attribute "data-increase-number" is a value for one increase iteration
     * */
    var initIncreaseNumber = function () {
        var $increaseNumber = $('[' + DATA_ATTR_INCREASE_NUMBER + ']'),
            startDate = START_DATE_INCREASE_NUMBER,
            nowDate = new Date(),
            numberOfDays = Math.floor((nowDate - startDate) / 1000 / 60 / 60 / 24);

        if ($increaseNumber.length) {
            $increaseNumber.each(function () {
                var $this = $(this),
                    step = +$this.attr(DATA_ATTR_INCREASE_NUMBER),
                    currentNumber = parseInt($this.text().replace(/\s/g, ''), 10),
                    newNumber = String(currentNumber + step * numberOfDays),
                    newNumberLength = newNumber.length,
                    numberOfIteration = Math.ceil(newNumberLength / 3),
                    splitArray = [],
                    i;

                for (i = 1; i < numberOfIteration; i++) {
                    splitArray.unshift(newNumber.substr(-3 * i, 3));
                }
                splitArray.unshift(newNumber.substr(0, newNumberLength - --i * 3));

                $this.text(splitArray.join(' '));
            });
        }
    };

    var timeUnits = function (number, type) {
        var stringOfNumber = String(number),
            lastDigit = stringOfNumber.substr(-1),
            timeUnit;

        if (lastDigit === '1' && number !== 11) {
            switch (type) {
                case 'days': timeUnit = 'день'; break;
                case 'hours': timeUnit = 'час'; break;
                case 'minutes': timeUnit = 'минута'; break;
                case 'seconds': timeUnit = 'секунда'; break;
            }
        } else if (/(2)|(3)|(4)/.test(lastDigit) && !/(12)|(13)|(14)/.test(stringOfNumber)) {
            switch (type) {
                case 'days': timeUnit = 'дня'; break;
                case 'hours': timeUnit = 'часа'; break;
                case 'minutes': timeUnit = 'минуты'; break;
                case 'seconds': timeUnit = 'секунды'; break;
            }
        } else {
            switch (type) {
                case 'days': timeUnit = 'дней'; break;
                case 'hours': timeUnit = 'часов'; break;
                case 'minutes': timeUnit = 'минут'; break;
                case 'seconds': timeUnit = 'секунд'; break;
            }
        }

        return timeUnit;
    };

    var startTimer = function () {
        var $days = $('[' + DATA_ATTR_TIMER_DAY + ']'),
            $hours = $('[' + DATA_ATTR_TIMER_HOUR + ']'),
            $minutes = $('[' + DATA_ATTR_TIMER_MIN + ']'),
            $seconds = $('[' + DATA_ATTR_TIMER_SEC + ']'),
            $daysUnit = $('[' + DATA_ATTR_TIMER_DAY_UNIT + ']'),
            $hoursUnit = $('[' + DATA_ATTR_TIMER_HOUR_UNIT + ']'),
            $minutesUnit = $('[' + DATA_ATTR_TIMER_MIN_UNIT + ']'),
            $secondsUnit = $('[' + DATA_ATTR_TIMER_SEC_UNIT + ']'),
            numberOfDays,
            numberOfHours,
            numberOfMinutes,
            numberOfSeconds,
            totalSeconds;

        function timer() {
            var days, hours, minutes, seconds;

            totalSeconds--;

            days = Math.floor(totalSeconds / (24 * 60 * 60));
            hours = Math.floor((totalSeconds - days * 24 * 60 * 60) / (60 * 60));
            minutes = Math.floor((totalSeconds - days * 24 * 60 * 60 - hours * 60 * 60) / 60);
            seconds = Math.floor(totalSeconds - days * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60);

            $days.text(days);
            $hours.text(hours < 10 ? '0' + hours : hours);
            $minutes.text(minutes < 10 ? '0' + minutes : minutes);
            $seconds.text(seconds < 10 ? '0' + seconds : seconds);

            $daysUnit.text(timeUnits(days, 'days'));
            $hoursUnit.text(timeUnits(hours, 'hours'));
            $minutesUnit.text(timeUnits(minutes, 'minutes'));
            $secondsUnit.text(timeUnits(seconds, 'seconds'));

            setTimeout(timer, 1000);
        }

        if ($days.length && $hours.length && $minutes.length && $seconds.length) {
            numberOfDays = +$days.text();
            numberOfHours = +$hours.text();
            numberOfMinutes = +$minutes.text();
            numberOfSeconds = +$seconds.text();
            totalSeconds = numberOfDays * 24 * 60 * 60 + numberOfHours * 60 * 60 + numberOfMinutes * 60 + numberOfSeconds;

            timer();
        }
    };

    var initTimer = function () {
        initIncreaseNumber();
        startTimer();
    };

    return {
        initTimer: initTimer
    }
})();
var scrollAnimation = (function () {
    "use strict";

    var DATA_ATTR_SCROLL_ANIMATION = 'data-scroll-animation',
        DATA_ATTR_SCROLL_ANIMATION_DELAY = 'data-scroll-animation-delay';

    /**
     * The value of animation delay must be from 000 through 1000 and
     * must be multiple of 100
     * */
    var initScrollAnimation = function () {
        var $scrollAnimation = $('[' + DATA_ATTR_SCROLL_ANIMATION + ']');

        if ($scrollAnimation.length) {
            $scrollAnimation.each(function () {
                var $this = $(this),
                    elemCoordBottom = $this[0].getBoundingClientRect().bottom,
                    windowHeight = $(window).height(),
                    elemHeight = $this.height(),
                    valueOfAnimationDelay = $this.attr(DATA_ATTR_SCROLL_ANIMATION_DELAY),
                    classNameAnimation = 'animation--' + $this.attr(DATA_ATTR_SCROLL_ANIMATION),
                    classNameAnimationDelay = valueOfAnimationDelay ?
                        'animation-delay--' + $this.attr(DATA_ATTR_SCROLL_ANIMATION_DELAY) + 'ms' : '';

                if (elemCoordBottom - elemHeight / 2 <= windowHeight) {
                    $this.addClass(classNameAnimation + ' ' + classNameAnimationDelay);
                }
            });
        }
    };

    return {
        initScrollAnimation: initScrollAnimation
    }
})();
var sticker = (function () {
    "use strict";

    var DATA_ATTR_STICKER = 'data-sticker',
        DATA_ATTR_STICKER_CLOSE = 'data-sticker-close',
        CLASS_NAME_OPEN = 'open',
        WRAPPER_WIDTH = 1280;

    var initSticker = function () {
        var $sticker = $('[' + DATA_ATTR_STICKER + ']'),
            windowWidth = window.innerWidth,
            documentWidth = document.documentElement.clientWidth,
            scrollBarWidth = windowWidth - documentWidth;

        if ($sticker.length) {
            $sticker.css({
                'top': $('[' + stickyHeader.DATA_ATTR_HEADER_TOP + ']').height() + 'px',
                'right': 'calc((100vw - ' + (WRAPPER_WIDTH + scrollBarWidth) + 'px) / 2)'
            });

            $('[' + DATA_ATTR_STICKER_CLOSE + ']').on('click', function () {
                $(this).parents('[' + DATA_ATTR_STICKER + ']').removeClass(CLASS_NAME_OPEN);
            });
        }
    };

    return {
        initSticker: initSticker
    }
})();
var collapse = (function () {
    "use strict";

    var DATA_ATTR_COLLAPSE = 'data-collapse',
        CLASS_NAME_ACTIVE = 'active';

    var initCollapse = function () {
        $('[' + DATA_ATTR_COLLAPSE + ']').on('click', function () {
            $(this).toggleClass(CLASS_NAME_ACTIVE);
        });
    };

    return {
        initCollapse: initCollapse
    }
})();
var scrollMenu = (function () {
    "use strict";

    var DATA_ATTR_SCROLL_MENU_LINK = 'data-scroll-menu-link',
        DATA_ATTR_SCROLL_MENU_CONTENT = 'data-scroll-menu-content',
        CLASS_NAME_ACTIVE = 'active',
        CLASS_NAME_HEADER_NAV = 'header-nav',
        ANIMATION_DURATION = 500;

    var showContent = function (sectionNumber) {
        var reqSection = $('[' + DATA_ATTR_SCROLL_MENU_CONTENT + ']')
                .filter('[' + DATA_ATTR_SCROLL_MENU_CONTENT + '="' + sectionNumber + '"]'),
            reqSectionPos = reqSection.offset().top - $('[' + stickyHeader.DATA_ATTR_HEADER_BOTTOM + ']')
                    .find('.' + CLASS_NAME_HEADER_NAV).height();

        $('body, html').animate({scrollTop: reqSectionPos}, ANIMATION_DURATION);
    };

    var scrollContent = function () {
        $('[' + DATA_ATTR_SCROLL_MENU_LINK + ']').on('click', function(event) {
            var $this = $(this);

            event.preventDefault();

            $this.addClass(CLASS_NAME_ACTIVE).siblings().removeClass(CLASS_NAME_ACTIVE);

            showContent($this.attr(DATA_ATTR_SCROLL_MENU_LINK));
        });
    };

    var checkContent = function () {
        $('[' + DATA_ATTR_SCROLL_MENU_CONTENT + ']').each(function() {
            var $this = $(this),
                topEdge = $this.offset().top - $('[' + stickyHeader.DATA_ATTR_HEADER_BOTTOM + ']')
                        .find('.' + CLASS_NAME_HEADER_NAV).height(),
                bottomEdge = topEdge + $this.height(),
                wScroll = $(window).scrollTop();

            if (topEdge < wScroll && bottomEdge > wScroll) {
                var currentId = $this.attr(DATA_ATTR_SCROLL_MENU_CONTENT),
                    reqLink = $('[' + DATA_ATTR_SCROLL_MENU_LINK + ']')
                        .filter('[' + DATA_ATTR_SCROLL_MENU_LINK + '="' + currentId + '"]');

                reqLink.addClass(CLASS_NAME_ACTIVE).siblings().removeClass(CLASS_NAME_ACTIVE);
            }
        });
    };

    return {
        scrollActiveContent: scrollContent,
        checkActiveContent: checkContent
    }
})();
var inputMask = (function () {
    "use strict";

    var DATA_ATTR_INPUTMASK_PHONE = 'data-inputmask-phone',
        DATA_ATTR_INPUTMASK_NAME = 'data-inputmask-name',
        ERROR_MESSAGE_PHONE = 'Укажите корректный номер телефона',
        ERROR_MESSAGE_NAME = 'Только русские буквы, до 50 символов',
        CLASS_NAME_INPUT_ERROR_MESSAGE = 'input-error-message';

    var setInputMask = function (selector, mask, errorMessage, placeholder) {
        $(selector).each(function () {
            var $this = $(this);

            $this.on('focus', function () {
                $this.next('.' + CLASS_NAME_INPUT_ERROR_MESSAGE).remove();
            });

            $this.inputmask({
                mask: mask,
                showMaskOnHover: false,
                placeholder: placeholder || '_',

                definitions: {
                    'r': {
                        validator: '[А-ЯЁа-яё ]',
                        cardinality: 1
                    }
                },

                onKeyValidation: function(key, result) {
                    if (!result && !$this.next('.' + CLASS_NAME_INPUT_ERROR_MESSAGE).length && !$this.inputmask('isComplete')) {
                        $this.after('<span class="' + CLASS_NAME_INPUT_ERROR_MESSAGE + '">' + errorMessage + '</span>');
                    }
                },

                onincomplete: function () {
                    if (!$this.next('.' + CLASS_NAME_INPUT_ERROR_MESSAGE).length) {
                        $this.after('<span class="' + CLASS_NAME_INPUT_ERROR_MESSAGE + '">' + errorMessage + '</span>');
                    }
                },

                onKeyDown: function () {
                    $this.next('.' + CLASS_NAME_INPUT_ERROR_MESSAGE).remove();
                }
            });
        });
    };

    var initInputMask = function () {
        setInputMask('[' + DATA_ATTR_INPUTMASK_PHONE + ']', '+375 (99) 999 99 99', ERROR_MESSAGE_PHONE);
        setInputMask('[' + DATA_ATTR_INPUTMASK_NAME + ']', 'r{1,50}', ERROR_MESSAGE_NAME, ' ');
    };

    return {
        initInputMask: initInputMask
    }
})();
var mobileMenu = (function () {
    "use strict";

    var DATA_ATTR_MOBILE_MENU = 'data-mobile-menu',
        DATA_ATTR_MOBILE_MENU_LINK = 'data-mobile-menu-link',
        CLASS_NAME_MOBILE_MENU_OPEN = 'mobile-menu-open';

    var initMobileMenu = function () {
        $('[' + DATA_ATTR_MOBILE_MENU_LINK + ']').on('click', function () {
            $(this).closest('[' + DATA_ATTR_MOBILE_MENU + ']').toggleClass(CLASS_NAME_MOBILE_MENU_OPEN);
        });
    };

    return {
        initMobileMenu: initMobileMenu
    }
})();
var browserDefinition = (function () {
    "use strict";

    var isIE = function () {
        return (/(MSIE 10|rv:11.0|Edge\/\d.)/i.test(navigator.userAgent));
    };

    return {
        isIE: isIE
    }
})();
var responsiveSvg = (function () {
    "use strict";

    var ID_RESPONSIVE_SVG = 'responsive-svg';

    var initResponsiveSvg = function () {
        if (browserDefinition.isIE()) {
            var $svg = $('#' + ID_RESPONSIVE_SVG),
                windowWidth = $(window).width(),
                svgWidth = 1000,
                svgHeight = 825,
                scale = 825 / 1000;

            if (windowWidth < 1000) {
                svgWidth = windowWidth;
                svgHeight = windowWidth * scale;
            }

            $svg.attr({
                'width': svgWidth + 'px',
                'height': svgHeight + 'px'
            });
        }
    };

    return {
        initResponsiveSvg: initResponsiveSvg
    }
})();
var additionalText = (function () {
    "use strict";

    var DATA_ATTR_ADDITIONAL_TEXT_WRAP = 'data-additional-text-wrap',
        DATA_ATTR_ADDITIONAL_TEXT = 'data-additional-text',
        DATA_ATTR_ADDITIONAL_LINK = 'data-additional-link',
        CLASS_NAME_ACTIVE = 'additional-link-active',
        CLASS_NAME_SHOW = 'show',
        SCREEN_WIDTH_MD = 768;

    var toogle = function () {
        if (window.innerWidth >= SCREEN_WIDTH_MD) {
            $('[' + DATA_ATTR_ADDITIONAL_TEXT_WRAP + ']').each(function () {
                var $this = $(this),
                    $text = $this.find('[' + DATA_ATTR_ADDITIONAL_TEXT + ']');

                if ($text.height() > $this.height()) {
                    $this.find('[' + DATA_ATTR_ADDITIONAL_LINK + ']').addClass(CLASS_NAME_SHOW);
                } else {
                    $this.find('[' + DATA_ATTR_ADDITIONAL_LINK + ']').removeClass(CLASS_NAME_SHOW);
                }
            });
        } else {
            $('[' + DATA_ATTR_ADDITIONAL_TEXT_WRAP + ']').each(function () {
                $(this).find('[' + DATA_ATTR_ADDITIONAL_LINK + ']').removeClass(CLASS_NAME_SHOW);
            });
        }
    };

    var initAdditionalText = function () {
        $('[' + DATA_ATTR_ADDITIONAL_LINK + ']').on('click', function () {
            $(this).parents('[' + DATA_ATTR_ADDITIONAL_TEXT_WRAP + ']').toggleClass(CLASS_NAME_ACTIVE);
        });

        toogle();
    };

    var toggleAditionalText =  function () {
        toogle();
    };

    return {
        initAdditionalText: initAdditionalText,
        toggleAditionalText: toggleAditionalText
    }
})();
var feedbackPopup = (function () {
    "use strict";

    var DATA_ATTR_FEEDBACK_POPUP = 'data-feedback-popup';

    var initPopup = function () {
        $('[' + DATA_ATTR_FEEDBACK_POPUP + ']').colorbox({rel:'gal'});
    };

    return {
        initPopup: initPopup
    }
})();
var app = (function () {
    "use strict";

    /**
     * Ready callback
     * */
    var documentReady = function () {
        responsiveSvg.initResponsiveSvg();
        dropdown.initDropdown();
        slider.initSlider();
        timer.initTimer();
        popup.initPopup();
        stickyHeader.initStickyHeader();
        scrollAnimation.initScrollAnimation();
        sticker.initSticker();
        map.initMap();
        collapse.initCollapse();
        scrollMenu.scrollActiveContent();
        scrollMenu.checkActiveContent();
        stickySidebar.initStickySidebar();
        inputMask.initInputMask();
        mobileMenu.initMobileMenu();
        additionalText.initAdditionalText();
        feedbackPopup.initPopup();
    };

    /**
     * Load callback
     * */
    var windowLoad = function () {

    };

    /**
     * Resize callback
     * */
    var windowResize = function () {
        stickyHeader.initStickyHeader();
        responsiveSvg.initResponsiveSvg();
        additionalText.toggleAditionalText();
    };

    /**
     * Scroll callback
     * */
    var windowScroll = function () {
        stickyHeader.initStickyHeader();
        scrollAnimation.initScrollAnimation();
        scrollMenu.checkActiveContent();
    };

    return {
        ready: documentReady,
        load: windowLoad,
        resize: windowResize,
        scroll: windowScroll
    }
})();

/**
 * Document ready
 * */
document.addEventListener('DOMContentLoaded', app.ready);

/**
 * Window load
 * */
window.addEventListener('load', app.load);

/**
 * Window resize
 * */
window.addEventListener('resize', app.resize);

/**
 * Window scroll
 * */
window.addEventListener('scroll', app.scroll);