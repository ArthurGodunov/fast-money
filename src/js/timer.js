var timer = (function () {
    "use strict";

    var DATA_ATTR_INCREASE_NUMBER = 'data-increase-number',
        DATA_ATTR_TIMER_DAY = 'data-timer-day',
        DATA_ATTR_TIMER_HOUR = 'data-timer-hour',
        DATA_ATTR_TIMER_MIN = 'data-timer-min',
        DATA_ATTR_TIMER_SEC = 'data-timer-sec',
        DATA_ATTR_TIMER_DAY_UNIT = 'data-timer-day-unit',
        DATA_ATTR_TIMER_HOUR_UNIT = 'data-timer-hour-unit',
        DATA_ATTR_TIMER_MIN_UNIT = 'data-timer-min-unit',
        DATA_ATTR_TIMER_SEC_UNIT = 'data-timer-sec-unit',
        START_DATE_INCREASE_NUMBER = new Date(2017, 6, 6, 0, 0, 0, 0); // 6 July 2017 00:00:00

    /**
     * The value of attribute "data-increase-number" is a value for one increase iteration
     * */
    var initIncreaseNumber = function () {
        var $increaseNumber = $('[' + DATA_ATTR_INCREASE_NUMBER + ']'),
            startDate = START_DATE_INCREASE_NUMBER,
            nowDate = new Date(),
            numberOfDays = Math.floor((nowDate - startDate) / 1000 / 60 / 60 / 24);

        if ($increaseNumber.length) {
            $increaseNumber.each(function () {
                var $this = $(this),
                    step = +$this.attr(DATA_ATTR_INCREASE_NUMBER),
                    currentNumber = parseInt($this.text().replace(/\s/g, ''), 10),
                    newNumber = String(currentNumber + step * numberOfDays),
                    newNumberLength = newNumber.length,
                    numberOfIteration = Math.ceil(newNumberLength / 3),
                    splitArray = [],
                    i;

                for (i = 1; i < numberOfIteration; i++) {
                    splitArray.unshift(newNumber.substr(-3 * i, 3));
                }
                splitArray.unshift(newNumber.substr(0, newNumberLength - --i * 3));

                $this.text(splitArray.join(' '));
            });
        }
    };

    var timeUnits = function (number, type) {
        var stringOfNumber = String(number),
            lastDigit = stringOfNumber.substr(-1),
            timeUnit;

        if (lastDigit === '1' && number !== 11) {
            switch (type) {
                case 'days': timeUnit = 'день'; break;
                case 'hours': timeUnit = 'час'; break;
                case 'minutes': timeUnit = 'минута'; break;
                case 'seconds': timeUnit = 'секунда'; break;
            }
        } else if (/(2)|(3)|(4)/.test(lastDigit) && !/(12)|(13)|(14)/.test(stringOfNumber)) {
            switch (type) {
                case 'days': timeUnit = 'дня'; break;
                case 'hours': timeUnit = 'часа'; break;
                case 'minutes': timeUnit = 'минуты'; break;
                case 'seconds': timeUnit = 'секунды'; break;
            }
        } else {
            switch (type) {
                case 'days': timeUnit = 'дней'; break;
                case 'hours': timeUnit = 'часов'; break;
                case 'minutes': timeUnit = 'минут'; break;
                case 'seconds': timeUnit = 'секунд'; break;
            }
        }

        return timeUnit;
    };

    var startTimer = function () {
        var $days = $('[' + DATA_ATTR_TIMER_DAY + ']'),
            $hours = $('[' + DATA_ATTR_TIMER_HOUR + ']'),
            $minutes = $('[' + DATA_ATTR_TIMER_MIN + ']'),
            $seconds = $('[' + DATA_ATTR_TIMER_SEC + ']'),
            $daysUnit = $('[' + DATA_ATTR_TIMER_DAY_UNIT + ']'),
            $hoursUnit = $('[' + DATA_ATTR_TIMER_HOUR_UNIT + ']'),
            $minutesUnit = $('[' + DATA_ATTR_TIMER_MIN_UNIT + ']'),
            $secondsUnit = $('[' + DATA_ATTR_TIMER_SEC_UNIT + ']'),
            numberOfDays,
            numberOfHours,
            numberOfMinutes,
            numberOfSeconds,
            totalSeconds;

        function timer() {
            var days, hours, minutes, seconds;

            totalSeconds--;

            days = Math.floor(totalSeconds / (24 * 60 * 60));
            hours = Math.floor((totalSeconds - days * 24 * 60 * 60) / (60 * 60));
            minutes = Math.floor((totalSeconds - days * 24 * 60 * 60 - hours * 60 * 60) / 60);
            seconds = Math.floor(totalSeconds - days * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60);

            $days.text(days);
            $hours.text(hours < 10 ? '0' + hours : hours);
            $minutes.text(minutes < 10 ? '0' + minutes : minutes);
            $seconds.text(seconds < 10 ? '0' + seconds : seconds);

            $daysUnit.text(timeUnits(days, 'days'));
            $hoursUnit.text(timeUnits(hours, 'hours'));
            $minutesUnit.text(timeUnits(minutes, 'minutes'));
            $secondsUnit.text(timeUnits(seconds, 'seconds'));

            setTimeout(timer, 1000);
        }

        if ($days.length && $hours.length && $minutes.length && $seconds.length) {
            numberOfDays = +$days.text();
            numberOfHours = +$hours.text();
            numberOfMinutes = +$minutes.text();
            numberOfSeconds = +$seconds.text();
            totalSeconds = numberOfDays * 24 * 60 * 60 + numberOfHours * 60 * 60 + numberOfMinutes * 60 + numberOfSeconds;

            timer();
        }
    };

    var initTimer = function () {
        initIncreaseNumber();
        startTimer();
    };

    return {
        initTimer: initTimer
    }
})();