var feedbackPopup = (function () {
    "use strict";

    var DATA_ATTR_FEEDBACK_POPUP = 'data-feedback-popup';

    var initPopup = function () {
        $('[' + DATA_ATTR_FEEDBACK_POPUP + ']').colorbox({rel:'gal'});
    };

    return {
        initPopup: initPopup
    }
})();