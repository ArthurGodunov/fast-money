var additionalText = (function () {
    "use strict";

    var DATA_ATTR_ADDITIONAL_TEXT_WRAP = 'data-additional-text-wrap',
        DATA_ATTR_ADDITIONAL_TEXT = 'data-additional-text',
        DATA_ATTR_ADDITIONAL_LINK = 'data-additional-link',
        CLASS_NAME_ACTIVE = 'additional-link-active',
        CLASS_NAME_SHOW = 'show',
        SCREEN_WIDTH_MD = 768;

    var toogle = function () {
        if (window.innerWidth >= SCREEN_WIDTH_MD) {
            $('[' + DATA_ATTR_ADDITIONAL_TEXT_WRAP + ']').each(function () {
                var $this = $(this),
                    $text = $this.find('[' + DATA_ATTR_ADDITIONAL_TEXT + ']');

                if ($text.height() > $this.height()) {
                    $this.find('[' + DATA_ATTR_ADDITIONAL_LINK + ']').addClass(CLASS_NAME_SHOW);
                } else {
                    $this.find('[' + DATA_ATTR_ADDITIONAL_LINK + ']').removeClass(CLASS_NAME_SHOW);
                }
            });
        } else {
            $('[' + DATA_ATTR_ADDITIONAL_TEXT_WRAP + ']').each(function () {
                $(this).find('[' + DATA_ATTR_ADDITIONAL_LINK + ']').removeClass(CLASS_NAME_SHOW);
            });
        }
    };

    var initAdditionalText = function () {
        $('[' + DATA_ATTR_ADDITIONAL_LINK + ']').on('click', function () {
            $(this).parents('[' + DATA_ATTR_ADDITIONAL_TEXT_WRAP + ']').toggleClass(CLASS_NAME_ACTIVE);
        });

        toogle();
    };

    var toggleAditionalText =  function () {
        toogle();
    };

    return {
        initAdditionalText: initAdditionalText,
        toggleAditionalText: toggleAditionalText
    }
})();