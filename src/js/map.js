var map = (function () {
    "use strict";

    var ID_REGIONS = '#region-minsk, #region-grodno, #region-brest, #region-gomel, #region-mogilev, #region-vitebsk',
        CLASS_NAME_HOVER_REGION = 'active';

    var initMap = function () {
        $(ID_REGIONS).each(function () {
            var $this = $(this);

            $this.hover(
                function () {
                    $this.addClass(CLASS_NAME_HOVER_REGION);
                },
                function () {
                    $this.removeClass(CLASS_NAME_HOVER_REGION);
                }
            );
        });
    };

    return {
        initMap: initMap
    }
})();