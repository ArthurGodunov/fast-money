var collapse = (function () {
    "use strict";

    var DATA_ATTR_COLLAPSE = 'data-collapse',
        CLASS_NAME_ACTIVE = 'active';

    var initCollapse = function () {
        $('[' + DATA_ATTR_COLLAPSE + ']').on('click', function () {
            $(this).toggleClass(CLASS_NAME_ACTIVE);
        });
    };

    return {
        initCollapse: initCollapse
    }
})();