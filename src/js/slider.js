var slider = (function () {
    "use strict";

    var DATA_ATTR_SLIDER = 'data-slider',
        SCREEN_LG = 1024;

    /**
     * @see https://github.com/stevenwanderski/bxslider-4
     * */
    var initBxSlider = function () {
        var $slider = $('[' + DATA_ATTR_SLIDER + ']');

        if ($slider.length) {
            $slider.bxSlider({
                speed: 500,
                auto: false,
                pause: 4000,
                pager: false,
                nextText: '',
                prevText: '',
                touchEnabled: $(window).width() < SCREEN_LG
            });
        }
    };

    return {
        initSlider: initBxSlider
    }
})();