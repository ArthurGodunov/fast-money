var scrollMenu = (function () {
    "use strict";

    var DATA_ATTR_SCROLL_MENU_LINK = 'data-scroll-menu-link',
        DATA_ATTR_SCROLL_MENU_CONTENT = 'data-scroll-menu-content',
        CLASS_NAME_ACTIVE = 'active',
        CLASS_NAME_HEADER_NAV = 'header-nav',
        ANIMATION_DURATION = 500;

    var showContent = function (sectionNumber) {
        var reqSection = $('[' + DATA_ATTR_SCROLL_MENU_CONTENT + ']')
                .filter('[' + DATA_ATTR_SCROLL_MENU_CONTENT + '="' + sectionNumber + '"]'),
            reqSectionPos = reqSection.offset().top - $('[' + stickyHeader.DATA_ATTR_HEADER_BOTTOM + ']')
                    .find('.' + CLASS_NAME_HEADER_NAV).height();

        $('body, html').animate({scrollTop: reqSectionPos}, ANIMATION_DURATION);
    };

    var scrollContent = function () {
        $('[' + DATA_ATTR_SCROLL_MENU_LINK + ']').on('click', function(event) {
            var $this = $(this);

            event.preventDefault();

            $this.addClass(CLASS_NAME_ACTIVE).siblings().removeClass(CLASS_NAME_ACTIVE);

            showContent($this.attr(DATA_ATTR_SCROLL_MENU_LINK));
        });
    };

    var checkContent = function () {
        $('[' + DATA_ATTR_SCROLL_MENU_CONTENT + ']').each(function() {
            var $this = $(this),
                topEdge = $this.offset().top - $('[' + stickyHeader.DATA_ATTR_HEADER_BOTTOM + ']')
                        .find('.' + CLASS_NAME_HEADER_NAV).height(),
                bottomEdge = topEdge + $this.height(),
                wScroll = $(window).scrollTop();

            if (topEdge < wScroll && bottomEdge > wScroll) {
                var currentId = $this.attr(DATA_ATTR_SCROLL_MENU_CONTENT),
                    reqLink = $('[' + DATA_ATTR_SCROLL_MENU_LINK + ']')
                        .filter('[' + DATA_ATTR_SCROLL_MENU_LINK + '="' + currentId + '"]');

                reqLink.addClass(CLASS_NAME_ACTIVE).siblings().removeClass(CLASS_NAME_ACTIVE);
            }
        });
    };

    return {
        scrollActiveContent: scrollContent,
        checkActiveContent: checkContent
    }
})();