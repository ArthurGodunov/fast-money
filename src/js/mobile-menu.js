var mobileMenu = (function () {
    "use strict";

    var DATA_ATTR_MOBILE_MENU = 'data-mobile-menu',
        DATA_ATTR_MOBILE_MENU_LINK = 'data-mobile-menu-link',
        CLASS_NAME_MOBILE_MENU_OPEN = 'mobile-menu-open';

    var initMobileMenu = function () {
        $('[' + DATA_ATTR_MOBILE_MENU_LINK + ']').on('click', function () {
            $(this).closest('[' + DATA_ATTR_MOBILE_MENU + ']').toggleClass(CLASS_NAME_MOBILE_MENU_OPEN);
        });
    };

    return {
        initMobileMenu: initMobileMenu
    }
})();