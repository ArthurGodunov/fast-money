var popup = (function () {
    "use strict";

    var DATA_ATTR_POPUP = 'data-popup',
        DATA_ATTR_POPUP_OVERLAY = 'data-popup-overlay',
        DATA_ATTR_POPUP_CLOSE = 'data-popup-close',
        DATA_ATTR_POPUP_LINK = 'data-popup-link',
        CLASS_NAME_OPEN = 'open';

    var openPopup = function (popup) {
        $('[' + DATA_ATTR_POPUP_LINK + ']').each(function () {
            $(this).on('click', function () {
                $('[' + DATA_ATTR_POPUP_OVERLAY + '="' + popup + '"]').addClass(CLASS_NAME_OPEN);
            });
        });
    };

    var initPopup = function () {
        var $popup = $('[' + DATA_ATTR_POPUP + ']');

        if ($popup.length) {
            $('[' + DATA_ATTR_POPUP_CLOSE + ']').on('click', function () {
                $(this).parents('[' + DATA_ATTR_POPUP_OVERLAY + ']').removeClass(CLASS_NAME_OPEN);
            });

            $popup.on('click', function (event) {
                event.stopPropagation();
            });

            $('[' + DATA_ATTR_POPUP_OVERLAY + ']').on('click', function () {
                $(this).removeClass(CLASS_NAME_OPEN);
            });

            $('[' + DATA_ATTR_POPUP_LINK + ']').each(function () {
                var $this = $(this),
                    popup = $this.attr(DATA_ATTR_POPUP_LINK);

                $this.on('click', function () {
                    $('[' + DATA_ATTR_POPUP_OVERLAY + '="' + popup + '"]').addClass(CLASS_NAME_OPEN);
                });
            });
        }
    };

    return {
        initPopup: initPopup,
        openPopup: openPopup
    }
})();