var browserDefinition = (function () {
    "use strict";

    var isIE = function () {
        return (/(MSIE 10|rv:11.0|Edge\/\d.)/i.test(navigator.userAgent));
    };

    return {
        isIE: isIE
    }
})();