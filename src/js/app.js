var app = (function () {
    "use strict";

    /**
     * Ready callback
     * */
    var documentReady = function () {
        responsiveSvg.initResponsiveSvg();
        dropdown.initDropdown();
        slider.initSlider();
        timer.initTimer();
        popup.initPopup();
        stickyHeader.initStickyHeader();
        scrollAnimation.initScrollAnimation();
        sticker.initSticker();
        map.initMap();
        collapse.initCollapse();
        scrollMenu.scrollActiveContent();
        scrollMenu.checkActiveContent();
        stickySidebar.initStickySidebar();
        inputMask.initInputMask();
        mobileMenu.initMobileMenu();
        additionalText.initAdditionalText();
        feedbackPopup.initPopup();
    };

    /**
     * Load callback
     * */
    var windowLoad = function () {

    };

    /**
     * Resize callback
     * */
    var windowResize = function () {
        stickyHeader.initStickyHeader();
        responsiveSvg.initResponsiveSvg();
        additionalText.toggleAditionalText();
    };

    /**
     * Scroll callback
     * */
    var windowScroll = function () {
        stickyHeader.initStickyHeader();
        scrollAnimation.initScrollAnimation();
        scrollMenu.checkActiveContent();
    };

    return {
        ready: documentReady,
        load: windowLoad,
        resize: windowResize,
        scroll: windowScroll
    }
})();

/**
 * Document ready
 * */
document.addEventListener('DOMContentLoaded', app.ready);

/**
 * Window load
 * */
window.addEventListener('load', app.load);

/**
 * Window resize
 * */
window.addEventListener('resize', app.resize);

/**
 * Window scroll
 * */
window.addEventListener('scroll', app.scroll);