var inputMask = (function () {
    "use strict";

    var DATA_ATTR_INPUTMASK_PHONE = 'data-inputmask-phone',
        DATA_ATTR_INPUTMASK_NAME = 'data-inputmask-name',
        ERROR_MESSAGE_PHONE = 'Укажите корректный номер телефона',
        ERROR_MESSAGE_NAME = 'Только русские буквы, до 50 символов',
        CLASS_NAME_INPUT_ERROR_MESSAGE = 'input-error-message';

    var setInputMask = function (selector, mask, errorMessage, placeholder) {
        $(selector).each(function () {
            var $this = $(this);

            $this.on('focus', function () {
                $this.next('.' + CLASS_NAME_INPUT_ERROR_MESSAGE).remove();
            });

            $this.inputmask({
                mask: mask,
                showMaskOnHover: false,
                placeholder: placeholder || '_',

                definitions: {
                    'r': {
                        validator: '[А-ЯЁа-яё ]',
                        cardinality: 1
                    }
                },

                onKeyValidation: function(key, result) {
                    if (!result && !$this.next('.' + CLASS_NAME_INPUT_ERROR_MESSAGE).length && !$this.inputmask('isComplete')) {
                        $this.after('<span class="' + CLASS_NAME_INPUT_ERROR_MESSAGE + '">' + errorMessage + '</span>');
                    }
                },

                onincomplete: function () {
                    if (!$this.next('.' + CLASS_NAME_INPUT_ERROR_MESSAGE).length) {
                        $this.after('<span class="' + CLASS_NAME_INPUT_ERROR_MESSAGE + '">' + errorMessage + '</span>');
                    }
                },

                onKeyDown: function () {
                    $this.next('.' + CLASS_NAME_INPUT_ERROR_MESSAGE).remove();
                }
            });
        });
    };

    var initInputMask = function () {
        setInputMask('[' + DATA_ATTR_INPUTMASK_PHONE + ']', '+375 (99) 999 99 99', ERROR_MESSAGE_PHONE);
        setInputMask('[' + DATA_ATTR_INPUTMASK_NAME + ']', 'r{1,50}', ERROR_MESSAGE_NAME, ' ');
    };

    return {
        initInputMask: initInputMask
    }
})();