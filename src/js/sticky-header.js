var stickyHeader = (function () {
    "use strict";

    var DATA_ATTR_HEADER_TOP = 'data-header-top',
        DATA_ATTR_HEADER_BOTTOM = 'data-header-bottom',
        CLASS_NAME_STICKY = 'sticky',
        SCREEN_WIDTH_LG = 1024;

    var initStickyHeader = function () {
        var $headerTop = $('[' + DATA_ATTR_HEADER_TOP + ']'),
            $headerBottom = $('[' + DATA_ATTR_HEADER_BOTTOM + ']'),
            headerTopHeight = $headerTop.height(),
            windowScrollY = window.pageYOffset;

        if ($(window).width() > SCREEN_WIDTH_LG - 1) {
            if (windowScrollY >= headerTopHeight) {
                $headerBottom.addClass(CLASS_NAME_STICKY);
            } else {
                $headerBottom.removeClass(CLASS_NAME_STICKY);
            }
        } else {
            $headerBottom.removeClass(CLASS_NAME_STICKY);
        }
    };

    return {
        DATA_ATTR_HEADER_TOP: DATA_ATTR_HEADER_TOP,
        DATA_ATTR_HEADER_BOTTOM: DATA_ATTR_HEADER_BOTTOM,
        initStickyHeader: initStickyHeader
    }
})();