var stickySidebar = (function () {
    "use strict";

    var DATA_STICKY_SIDEBAR = 'data-sticky-sidebar',
        CLASS_NAME_HEADER_NAV = 'header-nav';

    var initStickySidebar = function () {
        $('[' + DATA_STICKY_SIDEBAR + ']').each(function () {
            $(this).stick_in_parent({
                offset_top: $('[' + stickyHeader.DATA_ATTR_HEADER_BOTTOM + ']').find('.' + CLASS_NAME_HEADER_NAV).height() + 20
            });
        });
    };

    return {
        initStickySidebar: initStickySidebar
    }
})();