var scrollAnimation = (function () {
    "use strict";

    var DATA_ATTR_SCROLL_ANIMATION = 'data-scroll-animation',
        DATA_ATTR_SCROLL_ANIMATION_DELAY = 'data-scroll-animation-delay';

    /**
     * The value of animation delay must be from 000 through 1000 and
     * must be multiple of 100
     * */
    var initScrollAnimation = function () {
        var $scrollAnimation = $('[' + DATA_ATTR_SCROLL_ANIMATION + ']');

        if ($scrollAnimation.length) {
            $scrollAnimation.each(function () {
                var $this = $(this),
                    elemCoordBottom = $this[0].getBoundingClientRect().bottom,
                    windowHeight = $(window).height(),
                    elemHeight = $this.height(),
                    valueOfAnimationDelay = $this.attr(DATA_ATTR_SCROLL_ANIMATION_DELAY),
                    classNameAnimation = 'animation--' + $this.attr(DATA_ATTR_SCROLL_ANIMATION),
                    classNameAnimationDelay = valueOfAnimationDelay ?
                        'animation-delay--' + $this.attr(DATA_ATTR_SCROLL_ANIMATION_DELAY) + 'ms' : '';

                if (elemCoordBottom - elemHeight / 2 <= windowHeight) {
                    $this.addClass(classNameAnimation + ' ' + classNameAnimationDelay);
                }
            });
        }
    };

    return {
        initScrollAnimation: initScrollAnimation
    }
})();