var sticker = (function () {
    "use strict";

    var DATA_ATTR_STICKER = 'data-sticker',
        DATA_ATTR_STICKER_CLOSE = 'data-sticker-close',
        CLASS_NAME_OPEN = 'open',
        WRAPPER_WIDTH = 1280;

    var initSticker = function () {
        var $sticker = $('[' + DATA_ATTR_STICKER + ']'),
            windowWidth = window.innerWidth,
            documentWidth = document.documentElement.clientWidth,
            scrollBarWidth = windowWidth - documentWidth;

        if ($sticker.length) {
            $sticker.css({
                'top': $('[' + stickyHeader.DATA_ATTR_HEADER_TOP + ']').height() + 'px',
                'right': 'calc((100vw - ' + (WRAPPER_WIDTH + scrollBarWidth) + 'px) / 2)'
            });

            $('[' + DATA_ATTR_STICKER_CLOSE + ']').on('click', function () {
                $(this).parents('[' + DATA_ATTR_STICKER + ']').removeClass(CLASS_NAME_OPEN);
            });
        }
    };

    return {
        initSticker: initSticker
    }
})();