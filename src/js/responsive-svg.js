var responsiveSvg = (function () {
    "use strict";

    var ID_RESPONSIVE_SVG = 'responsive-svg';

    var initResponsiveSvg = function () {
        if (browserDefinition.isIE()) {
            var $svg = $('#' + ID_RESPONSIVE_SVG),
                windowWidth = $(window).width(),
                svgWidth = 1000,
                svgHeight = 825,
                scale = 825 / 1000;

            if (windowWidth < 1000) {
                svgWidth = windowWidth;
                svgHeight = windowWidth * scale;
            }

            $svg.attr({
                'width': svgWidth + 'px',
                'height': svgHeight + 'px'
            });
        }
    };

    return {
        initResponsiveSvg: initResponsiveSvg
    }
})();