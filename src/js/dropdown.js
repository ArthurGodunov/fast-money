var dropdown = (function () {
    "use strict";

    var DATA_ATTR_DROPDOWN = 'data-dropdown',
        DATA_ATTR_DROPDOWN_LINK = 'data-dropdown-link';

    var toogleDropdown = function () {
        var $link = $('[' + DATA_ATTR_DROPDOWN_LINK + ']');

        if ($link.length) {
            $link.on('click', function (event) {
                var $self = $(this);

                $self.parents('[' + DATA_ATTR_DROPDOWN + ']').toggleClass('open');

                event.stopPropagation();

                $('body').on('click.dropdown', function (eventBody) {
                    if (!$(eventBody.target).closest('[' + DATA_ATTR_DROPDOWN + ']').length) {
                        $self.closest('[' + DATA_ATTR_DROPDOWN + ']').removeClass('open');

                        $('body').off('click.dropdown');
                    }
                });
            });
        }
    };

    return {
        initDropdown: toogleDropdown
    }
})();