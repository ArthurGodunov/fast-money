'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');

module.exports = function(options){
    return function () {
        return gulp.src(options.src)
            .pipe(concat(options.outputFileName + '.js'))
            .pipe(gulp.dest(options.dest))
    };
};