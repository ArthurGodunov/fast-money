'use strict';

var browserSync = require('browser-sync').create();

module.exports = function(options){
    return function() {
        browserSync.init({
            port: options.port,
            server: options.server
        });

        browserSync.watch(options.watch).on('change', browserSync.reload);
    }
};