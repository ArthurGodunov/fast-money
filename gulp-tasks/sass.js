'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

module.exports = function(options){
    return function () {
        return gulp.src(options.src)
            .pipe(sass({
                outputStyle: 'expanded'
            }))
            .pipe(gulp.dest(options.dest))
    };
};