'use strict';

var gulp = require('gulp');
var pug = require('gulp-pug');

module.exports = function(options){
    return function () {
        return gulp.src(options.src)
            .pipe(pug({
                client: false,
                pretty: true
            }))
            .pipe(gulp.dest(options.dest))
    };
};