'use strict';

var gulp = require('gulp');
var clean = require('gulp-clean');

module.exports = function(options){
    return function () {
        return gulp.src(options.src, {read: false})
            .pipe(clean());
    };
};