'use strict';

var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');

module.exports = function(options){
    return function () {
        return gulp.src(options.src)
            .pipe(autoprefixer({
                browsers: ['last 5 versions']
            }))
            .pipe(gulp.dest(options.dest))
    };
};